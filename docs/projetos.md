# Projetos Desenvolvidos

Os projetos desenvolvidos na disciplinas podem ser visto no portfolio [aqui](https://unb-esw.gitlab.io/fga-pi2/projetos/). Abaixo listamos os projetos por semestre.

# Semestre 2024.2
- [Repositório Squad 10 -  EnergyDrive](https://gitlab.com/unb-esw/fga-pi2/semestre-2024-3/squad10)          
    - [Gitpage](https://template-relatorio-bd2bf7.gitlab.io/)

- [Repositório Squad 09 - Mini Estufa](https://gitlab.com/unb-esw/fga-pi2/semestre-2024-3/squad09) 
    - [Gitpage](https://documentacao-3a1568.gitlab.io/)
- [Repositório Squad 08 - Pet Feeder](https://gitlab.com/unb-esw/fga-pi2/semestre-2024-3/squad08) 
     - [Gitpage](https://documentacao-ad66ec.gitlab.io)
- [Repositório Squad 07 - Gotinho](https://gitlab.com/unb-esw/fga-pi2/semestre-2024-3/squad07) 
    - [Gitpage](https://documentacao-4b939b.gitlab.io)
- [Repositório Squad 06 - We drink](https://gitlab.com/unb-esw/fga-pi2/semestre-2024-3/squad06) 
    - [Gitpage](https://docs-7911d5.gitlab.io)
- [Repositório Squad 05 - Paraboia](https://gitlab.com/unb-esw/fga-pi2/semestre-2024-3/squad05) 
    - [Gitpage](https://unb-esw.gitlab.io/fga-pi2/semestre-2024-3/squad05/docs)
- [Repositório Squad 04 - SolarBayu](https://gitlab.com/unb-esw/fga-pi2/semestre-2024-3/squad04) 
    - [Gitpage](https://documentacao-relatorio-2d8035.gitlab.io/)
- [Repositório Squad 03 - Speed Chair](https://gitlab.com/unb-esw/fga-pi2/semestre-2024-3/squad03)
    -  [Gitpage](https://documentacao-089885.gitlab.io/)
- [Repositório Squad 02 - Meu norte](https://gitlab.com/unb-esw/fga-pi2/semestre-2024-3/squad2) 
     - [Gitpage](https://docs-9b751e.gitlab.io/)
- [Repositório Squad 01 eVTOL](https://gitlab.com/unb-esw/fga-pi2/semestre-2024-3/squad01) 
    - [Gitpage](https://docs-5b0a4d.gitlab.io/landing.html)
  

# Semestre 2024.1

## Grupos e Projetos

Abaixo estão listados os grupos e seus respectivos projetos para o semestre 2024.1. Cada link leva à página com a documentação completa, onde você pode encontrar detalhes sobre o andamento do projeto, desafios enfrentados, soluções implementadas e resultados alcançados.

-   **Organização 2024-1** - [Acesse no GitLab](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1){:target="\_blank"}

### Documentação dos Grupos 2024.1

1. **G01 - Ovnitrap**

    - [Documentação](https://ovnitrap-relatorio-fga-pi2-semestre-2024-1-g1-c48d33da1876a99c9.gitlab.io/projeto-estrutura/){:target="\_blank"}

2. **G02 - Esteira Inteligente**

    - [Documentação](https://esteira-inteligente-docs-fga-pi2-semestre-2024-1-4f19bee6666f37.gitlab.io/){:target="\_blank"}

3. **G03 - Tracker Solar**

    - [Documentação](https://lappis-unb.gitlab.io/fga-pi2/semestre-2024-1/grupo-03/docs/){:target="\_blank"}

4. **G04 - Helios CW1 - Limpador de Painéis Solares**

    - [Documentação](https://lappis-unb.gitlab.io/fga-pi2/semestre-2024-1/grupo-05/relatorio/){:target="\_blank"}

5. **G05 - Aquamático**

    - [Documentação](https://relatorio-fga-pi2-semestre-2024-1-grupo05-4beaf972d28f4e6162d41.gitlab.io/){:target="\_blank"}

6. **G06 - Fogão Inteligente**

    - [Documentação](https://docs-fga-pi2-semestre-2024-1-grupo-06-0b350550c6bd5168e7922a7d8.gitlab.io/){:target="\_blank"}

7. **G07 - Silo Air**

    - [Documentação](https://relatorio-fga-pi2-semestre-2024-1-grupo-07-e6e361cd1b5ee319a4be.gitlab.io/){:target="\_blank"}

8. **G08 - Scan Point**
    - [Documentação](https://lappis-unb.gitlab.io/fga-pi2/semestre-2024-1/grupo-08/scanpoint/){:target="\_blank"}

---

## Projetos Semestres Anteriores

Explore os projetos desenvolvidos nos semestres anteriores, incluindo detalhes sobre objetivos, metodologia e resultados alcançados. Esses projetos abrangem uma variedade de áreas e demonstram a aplicação prática dos conhecimentos adquiridos pelos estudantes.

-   [Landing Page Sobre os Projetos](https://lappis-unb.gitlab.io/fga-pi2/projetos/){:target="\_blank"}: Uma visão geral dos projetos realizados nos semestres anteriores, com links para os detalhes de cada projeto.

Todos os projetos desenvolvidos em semestres anteriores estão disponíveis para consulta no GitLab. Acesse para ver os códigos, documentação e relatórios completos de cada projeto.

-   [Repositórios dos Projetos Anteriores](https://gitlab.com/lappis-unb/fga-pi2){:target="\_blank"}

