# Templates

Aqui estão os templates que serão úteis para o desenvolvimento e documentação dos projetos:

-   [Template de Repositório](https://gitlab.com/fga-pi2/template-relatorio){:target="\_blank"}: Utilize este template para estruturar o repositório do seu projeto, garantindo uma organização adequada dos arquivos e documentação.
-   [Template de GitPage](https://template-relatorio-fga-pi2-887e658e805143cb7b484b78f0e84982d99d.gitlab.io){:target="\_blank"}: Este template ajuda na criação de páginas GitPage para a apresentação e documentação do projeto de forma visualmente atraente e acessível.

---

# Materiais de Apoio

Os materiais de apoio dos semestres anteriores estão disponíveis para consulta e podem ser extremamente úteis para o desenvolvimento dos projetos atuais. Acesse os links abaixo para visualizar e baixar os materiais:

-   [Material 2023-2](https://www.dropbox.com/scl/fo/371kbgimioxlceecn79l1/h?rlkey=yknxhbi1h8d0v9kf31vrywkhp&dl=0){:target="\_blank"}

---
